import RPi.GPIO as GPIO
import time
import tkinter as TK

# Declaring Variables
red = 7
morseCodeList = []

# Setting up GPIO pins
GPIO.setmode(GPIO.BOARD)
GPIO.setup(red, GPIO.OUT)

def setupMorseAlphabet():
    file_object = open("morsecode.txt", "r")
    fileContents = file_object.read()
    fileLines = fileContents.split("\n")

    for morseChar in fileLines:
        morseCodeList.append(morseChar.split("#")[1])
    
    file_object.close()

def convertCharToInt(char):
    if char == " ":
        return 26
    
    return ord(char) - 97

def blinkMorseChar(char):
    print(char + " " + morseCodeList[convertCharToInt(char)])
    code = morseCodeList[convertCharToInt(char)]
    
    for bit in code:
        if bit == ".":
            GPIO.output(red, True)
            time.sleep(0.5)
            GPIO.output(red, False)
            time.sleep(0.5)
        if bit == "-":
            GPIO.output(red, True)
            time.sleep(1)
            GPIO.output(red, False)
            time.sleep(1)      

def showMorseCode(text):
    for char in text:
        blinkMorseChar(char)

# Setting up GUI
win = TK.Tk()
win.title("Print morse code")

setupMorseAlphabet()

txtCode = TK.Entry(win)
txtCode.pack()

btnSubmit = TK.Button(win, text = "Show Morse Code", command = lambda: showMorseCode(txtCode.get()))
btnSubmit.pack()

win.mainloop()

# Clean up GPIO pins after use
GPIO.cleanup()